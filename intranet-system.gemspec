# frozen_string_literal: true

require 'date'
require_relative 'lib/intranet/system/version'

Gem::Specification.new do |s|
  s.name        = Intranet::System::NAME
  s.version     = Intranet::System::VERSION

  s.summary     = 'System module for the intranet.'
  s.homepage    = Intranet::System::HOMEPAGE_URL
  s.metadata    = { 'source_code_uri' => Intranet::System::SOURCES_URL }
  s.license     = 'MIT'

  s.author      = 'Ebling Mis'
  s.email       = 'ebling.mis@protonmail.com'

  # Make sure the gem is built from versioned files
  s.files         = `git ls-files -z`.split("\0").grep(/^spec|^lib|^README/)
  s.require_paths = %w[lib]

  s.required_ruby_version = '~> 3.0'

  s.add_runtime_dependency 'intranet-core', '~> 2.0', '>= 2.1.0'
end
