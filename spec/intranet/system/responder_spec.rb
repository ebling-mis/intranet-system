# frozen_string_literal: true

require 'intranet/core'
require 'intranet/logger'
require 'intranet/abstract_responder'
require 'intranet/system/responder'

RSpec.describe Intranet::System::Responder do
  it 'should inherit from Intranet::AbstractResponder' do
    expect(described_class.superclass).to eql(Intranet::AbstractResponder)
  end

  it 'should define its name, version and homepage' do
    expect { described_class.module_name }.not_to raise_error
    expect { described_class.module_version }.not_to raise_error
    expect { described_class.module_homepage }.not_to raise_error
  end

  before do
    logger = Intranet::Logger.new(Intranet::Logger::FATAL)
    @core = Intranet::Core.new(logger)

    provider = double('Intranet::System::Provider')
    @stats = {
      hostname: File.read('/etc/hostname').chomp,
      loadavg: [0.76, 0.61, 0.42],
      cpu: {
        model: 'Intel(R) Pentium(R) CPU G4400 @ 3.30GHz',
        nb_cores: 2
      },
      ram: { total: 4_014_243_840, used: 2_765_328_384 },
      swaps: {
        '/dev/sda3' => { total: 9_699_323_904, used: 464_449_536 },
        '/swapfile' => { total: 134_209_536, used: 38_797_312 }
      },
      partitions: {
        '/boot/efi' => { total: 509_640_704, used: 135_168 },
        '/' => { total: 58_306_199_552, used: 11_480_780_800 },
        '/home' => { total: 884_998_905_856, used: 248_169_803_776 },
        '/media/user/disk' => { total: 1_000_202_039_296, used: 674_241_961_984 }
      }
    }
    allow(provider).to receive(:stats).and_return(@stats)

    @responder = described_class.new(provider)
    @core.register_module(
      @responder, ['system'], File.absolute_path('../../../lib/intranet/resources', __dir__)
    )
  end

  describe '#in_menu?' do
    it 'should return the value provided at initialization' do
      expect(described_class.new(nil, false).in_menu?).to be false
      expect(described_class.new(nil, true).in_menu?).to be true
    end
  end

  describe '#resources_dir' do
    it 'should return the absolute path of the resources directory' do
      expect(described_class.new(nil, false).resources_dir).to eql(
        File.absolute_path('../../../lib/intranet/resources', __dir__)
      )
    end
  end

  describe '#title' do
    it 'should return the title of the webpage provided by the module' do
      expect(@responder.title).to eql(I18n.t('system.monitor'))
    end
  end

  describe '#css_dependencies' do
    it 'should return the list of CSS dependencies' do
      expect(@responder.css_dependencies).to include('design/style.css')
    end
  end

  describe '#js_dependencies' do
    it 'should return the list of JavaScript dependencies' do
      expect(@responder.js_dependencies).to include('design/jsystem.js')
      expect(@responder.js_dependencies).to include('i18n.js')
    end
  end

  describe '#generate_page' do
    context 'when asked for \'/index.html\'' do
      it 'should return a partial HTML content with a loader' do
        code, mime, content = @responder.generate_page('/index.html', {})
        expect(code).to eql(206)
        expect(mime).to eql('text/html')
        expect(content).to eql(
          {
            content: "<section>\n<h2>#{I18n.t('system.monitor')}</h2>\n" \
                     "<ul class='breadcrumb'>\n" \
                     "<li>\n<a href='/index.html'>#{I18n.t('nav.home')}</a>\n</li>\n" \
                     "<li>#{I18n.t('system.monitor')}</li>\n" \
                     "</ul>\n\n" \
                     "<h3 id='cpu_model'>#{I18n.t('system.processor')} : </h3>\n" \
                     "<p class='meter' id='load1min'>\n<span class='desc'>1 min</span>\n<span cla" \
                     "ss='info'>0.00</span>\n<meter high='90' low='80' max='100' min='0' optimum=" \
                     "'50' title='0 %' value='0'></meter>\n</p>\n" \
                     "<p class='meter' id='load5min'>\n<span class='desc'>5 min</span>\n<span cla" \
                     "ss='info'>0.00</span>\n<meter high='90' low='80' max='100' min='0' optimum=" \
                     "'50' title='0 %' value='0'></meter>\n</p>\n" \
                     "<p class='meter' id='load15min'>\n<span class='desc'>15 min</span>\n<span c" \
                     "lass='info'>0.00</span>\n<meter high='90' low='80' max='100' min='0' optimu" \
                     "m='50' title='0 %' value='0'></meter>\n</p>\n" \
                     "<h3>#{I18n.t('system.memory')}</h3>\n" \
                     "<p class='meter' id='ram'>\n<span class='desc'>RAM</span>\n<span class='inf" \
                     "o'>0 M#{I18n.t('system.byte_abbrev')} #{I18n.t('system.out_of')} 0 M" \
                     "#{I18n.t('system.byte_abbrev')}</span>\n<meter high='90' low='80' max='100'" \
                     " min='0' optimum='50' title='0 %' value='0'></meter>\n</p>\n" \
                     "<div class='loading' id='swaps'></div>\n" \
                     "<h3>#{I18n.t('system.storage')}</h3>\n" \
                     "<div class='loading' id='storage'></div>\n" \
                     "</section>\n",
            title: I18n.t('system.monitor')
          }
        )
      end
    end

    context 'when asked for \'/api\'' do
      it 'should return a JSON representation of the system statistics' do
        code, mime, content = @responder.generate_page('/api', {})
        expect(code).to eql(200)
        expect(mime).to eql('application/json')
        expect(content).to eql(@stats.to_json)
      end
    end

    context 'when asked for \'/i18n.js\'' do
      it 'should return the internationalized version of required JavaScript variables' do
        code, mime, content = @responder.generate_page('/i18n.js', {})
        expect(code).to eql(200)
        expect(mime).to eql('text/javascript')
        expect(content).to eql(
          "window.byte_abbrev = '#{I18n.t('system.byte_abbrev')}';\n" \
          "window.out_of = '#{I18n.t('system.out_of')}';\n"
        )
      end
    end

    context 'otherwise' do
      it 'should return an HTTP 404 error' do
        expect(@responder.generate_page('index.html', {})).to eql([404, '', ''])
      end
    end
  end
end
