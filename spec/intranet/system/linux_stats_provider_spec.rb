# frozen_string_literal: true

require 'intranet/system/linux_stats_provider'

RSpec.describe Intranet::System::LinuxStatsProvider do
  before do
    expect(described_class::CPUINFO_FILE).to eql('/proc/cpuinfo')
    stub_const('Intranet::System::LinuxStatsProvider::CPUINFO_FILE',
               File.join(__dir__, 'sample_cpuinfo.txt'))

    expect(described_class::LOADAVG_FILE).to eql('/proc/loadavg')
    stub_const('Intranet::System::LinuxStatsProvider::LOADAVG_FILE',
               File.join(__dir__, 'sample_loadavg.txt'))

    expect(described_class::MEMINFO_FILE).to eql('/proc/meminfo')
    stub_const('Intranet::System::LinuxStatsProvider::MEMINFO_FILE',
               File.join(__dir__, 'sample_meminfo.txt'))

    expect(described_class::SWAPS_FILE).to eql('/proc/swaps')
    stub_const('Intranet::System::LinuxStatsProvider::SWAPS_FILE',
               File.join(__dir__, 'sample_swaps.txt'))

    allow(Time).to receive(:now).and_return(0)

    @provider = Intranet::System::LinuxStatsProvider.new
    allow(@provider).to receive(:`).with('df -lB1').and_return(
      File.read(File.join(__dir__, 'sample_df-lB1_1.txt'))
    )
  end

  describe '#stats' do
    it 'should return the latest system statistics' do
      expected1 = {
        hostname: File.read('/etc/hostname').chomp,
        loadavg: [0.76, 0.61, 0.42],
        cpu: {
          model: 'Intel(R) Pentium(R) CPU G4400 @ 3.30GHz',
          nb_cores: 2
        },
        ram: { total: 4_014_243_840, used: 2_765_328_384 },
        swaps: {
          '/dev/sda3' => { total: 9_699_323_904, used: 464_449_536 },
          '/swapfile' => { total: 134_209_536, used: 38_797_312 }
        },
        partitions: {
          '/boot/efi' => { total: 509_640_704, used: 135_168 },
          '/' => { total: 58_306_199_552, used: 11_480_780_800 },
          '/home' => { total: 884_998_905_856, used: 248_169_803_776 },
          '/media/user/disk' => { total: 1_000_202_039_296, used: 674_241_961_984 }
        }
      }
      expected2 = {
        hostname: File.read('/etc/hostname').chomp,
        loadavg: [0.76, 0.61, 0.42],
        cpu: {
          model: 'Intel(R) Pentium(R) CPU G4400 @ 3.30GHz',
          nb_cores: 2
        },
        ram: { total: 4_014_243_840, used: 2_765_328_384 },
        swaps: {
          '/dev/sda3' => { total: 9_699_323_904, used: 464_449_536 },
          '/swapfile' => { total: 134_209_536, used: 38_797_312 }
        },
        partitions: {
          '/boot/efi' => { total: 509_640_704, used: 135_168 },
          '/' => { total: 58_306_199_552, used: 11_480_780_800 },
          '/home' => { total: 884_998_905_856, used: 248_169_803_776 }
        }
      }

      expect(@provider.stats).to eql(expected1)

      # Modify df return to remove a disk
      allow(@provider).to receive(:`).with('df -lB1').and_return(
        File.read(File.join(__dir__, 'sample_df-lB1_2.txt'))
      )

      allow(Time).to receive(:now).and_return(4)

      # Check that output is not modified: system statistics are still valid
      expect(@provider.stats).to eql(expected1)

      allow(Time).to receive(:now).and_return(5)

      # Check that output is modified: system statistics have expired
      expect(@provider.stats).to eql(expected2)
    end
  end
end
