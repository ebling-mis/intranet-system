Filesystem         1B-blocks         Used    Available Use% Mounted on
udev              4108283904            0   4108283904   0% /dev
tmpfs              824324096     18341888    805982208   3% /run
/dev/sda2        58306199552  11480780800  43833151488  21% /
tmpfs             4121620480     49844224   4071776256   2% /dev/shm
tmpfs                5242880         4096      5238784   1% /run/lock
tmpfs             4121620480            0   4121620480   0% /sys/fs/cgroup
/dev/loop0          86114304     86114304            0 100% /snap/core18/2751
/dev/sda1          509640704       135168    509505536   1% /boot/efi
/dev/sdb4       884998905856 248169803776 591802150912  30% /home
/dev/sdc1      1000202039296 674241961984 325960077312  68% /media/user/disk
/dev/loop1          55967744     55967744            0 100% /snap/snapd/19457
tmpfs              824324096        16384    824307712   1% /run/user/115
tmpfs              824324096        53248    824270848   1% /run/user/1000
