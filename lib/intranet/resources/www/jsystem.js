/**
 * jsystem.js
 * JavaScript functions for the system monitor.
 */
"use strict";

const refreshIntervalMs = 5000;

const orderToString = ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi', 'Yi'];

function humanizeSize(size, order) {
  if (size < 1024)
    return Math.ceil(size) + ' ' + orderToString[order] + byte_abbrev;
  else if (size < 10 * 1024)
    return Math.ceil(size / 102.4) / 10 + ' ' + orderToString[order + 1] + byte_abbrev;
  else
    return humanizeSize(size / 1024, order + 1)
}

function updateMarkdownContent(json) {
  /* CPU */
  const cpuNumber = json.cpu.nb_cores;

  if (document.getElementById('cpu_model').textContent.endsWith(' : ')) {
      document.getElementById('cpu_model').textContent += json.cpu.model;
  }

  const loadAvgTriplet = ['load1min', 'load5min', 'load15min'];
  for (let i = 0; i < loadAvgTriplet.length; i++) {
    const load = json.loadavg[i];
    document.getElementById(loadAvgTriplet[i]).getElementsByClassName('info')[0].textContent = load.toFixed(2);
    document.getElementById(loadAvgTriplet[i]).getElementsByTagName('meter')[0].max = 100 * cpuNumber;
    document.getElementById(loadAvgTriplet[i]).getElementsByTagName('meter')[0].optimum = 50 * cpuNumber;
    document.getElementById(loadAvgTriplet[i]).getElementsByTagName('meter')[0].low = 80 * cpuNumber;
    document.getElementById(loadAvgTriplet[i]).getElementsByTagName('meter')[0].high = 90 * cpuNumber;
    document.getElementById(loadAvgTriplet[i]).getElementsByTagName('meter')[0].value = Math.ceil(100 * load);
    document.getElementById(loadAvgTriplet[i]).getElementsByTagName('meter')[0].title = Math.ceil(100 * load) + ' %';
  }

  /* RAM */
  const percent = Math.ceil(100 * json.ram.used / json.ram.total);
  const string = humanizeSize(json.ram.used, 0) + ' ' + out_of + ' ' + humanizeSize(json.ram.total, 0);
  document.getElementById('ram').getElementsByClassName('info')[0].textContent = string;
  document.getElementById('ram').getElementsByTagName('meter')[0].value = percent;
  document.getElementById('ram').getElementsByTagName('meter')[0].title = percent + ' %';

  /* SWAPs */
  var swapHtml = '';
  for (const [mountpoint, swap] of Object.entries(json.swaps)) {
    const percent = Math.ceil(100 * swap.used / swap.total);
    const string = humanizeSize(swap.used, 0) + ' ' + out_of + ' ' + humanizeSize(swap.total, 0);
    swapHtml += '<p class="meter"><span class="desc">Swap : ' + mountpoint + '</span><span class="info">' + string + '</span>';
    swapHtml += '<meter value="' + percent + '" min="0" max="100" low="80" high="90" optimum="50" title="' + percent + ' %">' + percent + '%</meter></p>';
  }
  document.getElementById('swaps').classList.remove('loading');
  document.getElementById('swaps').innerHTML = swapHtml;

  /* Partitions */
  var storageHtml = '';
  for (const [mountpoint, partition] of Object.entries(json.partitions)) {
    const percent = Math.ceil(100 * partition.used / partition.total);
    const string = humanizeSize(partition.used, 0) + ' ' + out_of + ' ' + humanizeSize(partition.total, 0);
    storageHtml += '<p class="meter"><span class="desc">' + mountpoint + '</span><span class="info">' + string + '</span>';
    storageHtml += '<meter value="' + percent + '" min="0" max="100" low="80" high="90" optimum="50" title="' + percent + ' %">' + percent + '%</meter></p>';
  }
  document.getElementById('storage').classList.remove('loading');
  document.getElementById('storage').innerHTML = storageHtml;
}

function updateSystemStats() {
  fetch('api')
    .then(response => response.json())
    .then(data => updateMarkdownContent(data))
}

/* Code executed after page load */
updateSystemStats();
setInterval(updateSystemStats, refreshIntervalMs);
