# frozen_string_literal: true

# The main Intranet namespace.
module Intranet
  # The System monitor module for the Intranet.
  module System
    # The name of the gem.
    NAME = 'intranet-system'

    # The version of the gem, according to semantic versionning.
    VERSION = '1.3.0'

    # The URL of the gem homepage.
    HOMEPAGE_URL = 'https://rubygems.org/gems/intranet-system'

    # The URL of the gem source code.
    SOURCES_URL = 'https://bitbucket.org/ebling-mis/intranet-system'
  end
end
