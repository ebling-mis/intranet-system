# frozen_string_literal: true

require 'intranet/abstract_responder'
require 'intranet/core/haml_wrapper'
require 'intranet/core/locales'
require_relative 'version'
require 'json'

module Intranet
  module System
    # The responder for the System monitor module of the Intranet.
    class Responder < AbstractResponder
      include Core::HamlWrapper # 'inherits' from methods of HamlWrapper

      # Returns the name of the module.
      # @return [String] The name of the module
      def self.module_name
        NAME
      end

      # The version of the module, according to semantic versionning.
      # @return [String] The version of the module
      def self.module_version
        VERSION
      end

      # The homepage of the module.
      # @return [String] The homepage URL of the module.
      def self.module_homepage
        HOMEPAGE_URL
      end

      # Initializes a new System responder instance.
      # @param provider [Object] The system statistics provider, responding to +#stats+.
      # @param in_menu [Boolean] Whether the module instance should be displayed in the main
      #                          navigation menu or not.
      def initialize(provider, in_menu = true)
        @provider = provider
        @in_menu = in_menu
      end

      # Specifies if the responder instance should be displayed in the main navigation menu or not.
      # @return [Boolean] True if the responder instance should be added to the main navigation
      #                   menu, False otherwise.
      def in_menu?
        @in_menu
      end

      # Specifies the absolute path to the resources directory for that module.
      # @return [String] The absolute path to the resources directory for the module.
      def resources_dir
        File.absolute_path(File.join('..', 'resources'), __dir__)
      end

      # Generates the HTML content associated to the given +path+ and +query+.
      # === REST API Description:
      # Read-only access to system statistics under +/api+ using +GET+ method. Response is in JSON
      # format with the following structure:
      #   {
      #     "hostname": "trantor",
      #     "loadavg": [0.42,0.48,0.56],
      #     "cpu": {
      #       "model": "Intel(R) Core(TM) i5-6500 CPU @ 3.20GHz",
      #       "nb_cores": 4
      #     },
      #     "ram": { "total": 8243249152, "used": 3149385728 },
      #     "swaps": {
      #       "/dev/sdb1": { "total": 19999485952, "used": 0 }
      #     },
      #     "partitions": {
      #       "/boot/efi": { "total": 509640704, "used": 135168 },
      #       "/": { "total": 58306199552, "used": 11482054656 }
      #     }
      #   }
      # @param path [String] The requested URI, relative to that module root URI.
      # @param query [Hash] The URI variable/value pairs, if any.
      # @return [Array] The HTTP return code, the MIME type and the answer body.
      def generate_page(path, query)
        case path
        when %r{^/index\.html$}
          return 206, 'text/html', generate_home_html
        when %r{^/api$}
          return 200, 'application/json', @provider.stats.to_json
        when %r{^/i18n\.js$}
          return 200, 'text/javascript', generate_i18n_js
        end
        super(path, query)
      end

      # The title of the System monitor module, as displayed on the web page.
      # @return [String] The title of the System monitor web page.
      def title
        I18n.t('system.monitor')
      end

      # Provides the list of Cascade Style Sheets (CSS) dependencies for this module.
      # @return [Array] The list of CSS dependencies.
      def css_dependencies
        super + ['design/style.css']
      end

      # Provides the list of Javascript files (JS) dependencies for this module.
      # @return [Array] The list of JS dependencies.
      def js_dependencies
        super + ['i18n.js', 'design/jsystem.js']
      end

      private

      def generate_home_html
        content = to_markup('system_home',
                            nav: { I18n.t('nav.home') => '/index.html', title => nil })
        { content: content, title: title }
      end

      def generate_i18n_js
        "window.byte_abbrev = '#{I18n.t('system.byte_abbrev')}';\n" \
          "window.out_of = '#{I18n.t('system.out_of')}';\n"
      end
    end
  end
end
